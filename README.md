# Desafio React Native - ioasys

Este documento `README.md` tem como objetivo de justificar as bibliotecas utilizadas no projeto e passar instruções de como executar a aplicação.

---

### Bibliotecas Utilizadas

- **axios** -> Utilizada para executar requisições na API.
- **react-navigation** -> Utilizada para criar o sistema de navegação e rotas no aplicativo, com a instalação desta, adiciona-se outras dependencias: `@react-native-community/masked-view`, `@react-navigation/native`, `@react-navigation/stack`, `react-native-gesture-handle`, `react-native-safe-area-context`, `react-native-screens` .
- **react-native-vector-icons** -> Utilizada para adicionar icones aos componentes e páginas, a biblioteca possui uma ampla coleção de icones.
- **redux, react-redux, redux-saga** -> Utilizada para o controle de estados globais da aplicação, recebendo dados da API e compartilhando com os estados entre páginas e componentes.
- **styled-components** -> Utilizada para facilitar no desenvolvimento do CSS e deixar o código da aplicação mais legível e simples.
- **@react-native-community/async-storage** -> Utilizada para armazenar dados de login na memoria do aplicativo.

### Instruções para execução

**Alguns requisitos:** versão do npm `6.14.4` e node.js `v12.18.0`, emulador ou aparelho(no modo desenvolvedor) Android na versão 6.0 ou mais.

- 1 - Na pasta raiz do codigo fonte, através do terminal do vsCode, digitar o comando `npm install`;
- 2 - Em seguida digitar `npx react-native run-android`, e aguardar enquanto o aplicativo é instalado no emulador ou aparelho físico.

### Funcionalidades concluídas

- Login no aplicativo, através do endpoint de autenticação.
- Opção para lembrar dados do login.
- Acesso a lista de empresas.
- Pesquisa por nome e filtro de tipo de empresa.
- Ao clicar no item da lista de empresas o aplicativo mostra os dados da empresa detalhadamente.
- Logout;

### Funcionalidades que eu gostaria de ter implementado

- Seleção de tema light/dark.
- Testes unitários.
