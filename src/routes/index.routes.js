import React from 'react';

import {useSelector} from 'react-redux';
import Auth from './auth.index.routes';
import NoAuth from './noAuth.index.routes';

const Routes = () => {
  const auth = useSelector(state => state.auth);
  const isLoged =
    auth.access_token !== '' && auth.client !== '' && auth.uid !== '';

  return isLoged ? <Auth /> : <NoAuth />;
};

export default Routes;
