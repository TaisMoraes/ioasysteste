import React, {useContext} from 'react';
import {StatusBar, TouchableOpacity} from 'react-native';
import {createStackNavigator} from '@react-navigation/stack';
import Search from '../pages/search';
import Enterprise from '../pages/enterprise';
import {ThemeContext} from 'styled-components';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {Creators as AuthActions} from '../store/ducks/auth';
import {useDispatch} from 'react-redux';
import IconHeader from '../components/IconHeader';

const Stack = createStackNavigator();

const Auth = () => {
  const theme = useContext(ThemeContext);
  const dispatch = useDispatch();
  const screenStyle = {
    headerTintColor: theme.colors.dark_text,
    headerStyle: {
      backgroundColor: theme.colors.background,
      elevation: 0,
    },
    headerTitleStyle: {
      fontSize: 16,
      alignSelf: 'center',
      fontFamily: theme.fontFamily.Bold,
    },
  };

  return (
    <>
      <StatusBar
        barStyle={'dark-content'}
        backgroundColor={theme.colors.background}
      />
      <Stack.Navigator screenOptions={screenStyle}>
        <Stack.Screen
          name="Pesquisa Empresas"
          component={Search}
          options={{
            title: 'HOME',
            headerLeft: () => <IconHeader />,
            headerRight: () => (
              <TouchableOpacity
                style={{marginRight: 10}}
                onPress={() => dispatch(AuthActions.sagaLogout())}>
                <Icon name="logout" size={25} color={'#000'} />
              </TouchableOpacity>
            ),
          }}
        />
        <Stack.Screen
          name="Empresa"
          component={Enterprise}
          options={{
            title: 'DADOS DA EMPRESA',
            headerRight: () => <IconHeader />,
          }}
        />
      </Stack.Navigator>
    </>
  );
};

export default Auth;
