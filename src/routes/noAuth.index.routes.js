import React, {useContext} from 'react';
import {StatusBar} from 'react-native';
import {createStackNavigator} from '@react-navigation/stack';
import Login from '../pages/login';
import {ThemeContext} from 'styled-components';

const Stack = createStackNavigator();

const NoAuth = () => {
  const theme = useContext(ThemeContext);
  return (
    <>
      <StatusBar
        barStyle={'light-content'}
        backgroundColor={theme.colors.quaternary}
      />
      <Stack.Navigator headerMode={'none'}>
        <Stack.Screen name="Login" component={Login} />
      </Stack.Navigator>
    </>
  );
};

export default NoAuth;
