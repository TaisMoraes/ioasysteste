import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import Routes from './routes/index.routes';
import store from './store';
import {Provider} from 'react-redux';
import * as themes from './styles/themes';
import {ThemeProvider} from 'styled-components';

const App = () => {
  return (
    <Provider store={store}>
      <ThemeProvider theme={themes.Light}>
        <NavigationContainer>
          <Routes />
        </NavigationContainer>
      </ThemeProvider>
    </Provider>
  );
};

export default App;
