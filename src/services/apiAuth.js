import {api} from './apiUrls';

export async function apiAuth(email, password) {
  let body = {email: email, password: password};

  return await api
    .post('/users/auth/sign_in', JSON.stringify(body), {
      headers: {'Content-Type': 'application/json'},
    })
    .then(response => response)
    .catch(error => error.response.data.errors[0]);
}
