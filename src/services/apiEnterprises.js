import {api} from './apiUrls';

export async function apiGetEnterprises(access_token, client, uid) {
  return await api
    .get('/enterprises', {
      headers: {
        'Content-Type': 'application/json',
        'access-token': access_token,
        client: client,
        uid: uid,
      },
    })
    .then(response => response.data)
    .catch(error => error.response.data.errors[0]);
}

export async function apiGetEnterpriseByID(access_token, client, uid, id) {
  return await api
    .get(`/enterprises/${id}`, {
      headers: {
        'Content-Type': 'application/json',
        'access-token': access_token,
        client: client,
        uid: uid,
      },
    })
    .then(response => response.data)
    .catch(error => error.response.data.errors[0]);
}

export async function apiGetEnterpriseFilter(
  access_token,
  client,
  uid,
  types,
  name,
) {
  let url = '/enterprises';

  (types !== null || name !== '') && (url = url + '?');
  types !== null && (url = url + `enterprise_types=${types}`);
  types !== null && name !== '' && (url = url + '&');
  name !== '' && (url = url + `name=${name}`);

  return await api
    .get(url, {
      headers: {
        'Content-Type': 'application/json',
        'access-token': access_token,
        client: client,
        uid: uid,
      },
    })
    .then(response => response.data)
    .catch(error => error.response.data.errors[0]);
}
