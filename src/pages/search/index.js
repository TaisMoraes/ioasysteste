import React, {useContext, useEffect} from 'react';
import {View, FlatList} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {ThemeContext} from 'styled-components';
import EnterpriseItem from '../../components/EnterpriseItem';
import Filter from '../../components/Filter';
import LoadingData from '../../components/LoadingData';
import SearchText from '../../components/SearchText';
import {Creators as EnterprisesActions} from '../../store/ducks/enterprises';

const Search = ({navigation}) => {
  const enterprises = useSelector(state => state.enterprises.data);
  const loading = useSelector(state => state.enterprises.loading);
  const {name, enterprise_type} = useSelector(state => state.search);
  const dispatch = useDispatch();
  const theme = useContext(ThemeContext);

  useEffect(() => {
    dispatch(EnterprisesActions.sagaRequest());
  }, [dispatch, name, enterprise_type]);

  return loading ? (
    <LoadingData />
  ) : (
    <View style={{backgroundColor: theme.colors.background, flex: 1}}>
      <SearchText />
      <Filter />
      <View
        style={{backgroundColor: theme.colors.background_secondary, flex: 1}}>
        <FlatList
          data={enterprises}
          renderItem={({item}) => (
            <EnterpriseItem item={item} navigation={navigation} />
          )}
          keyExtractor={item => item.id}
        />
      </View>
    </View>
  );
};

export default Search;
