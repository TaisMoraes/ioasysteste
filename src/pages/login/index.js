import React, {useContext, useState, useEffect, useRef} from 'react';
import {TouchableOpacity, ImageBackground, Switch} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {useDispatch, useSelector} from 'react-redux';
import {ThemeContext} from 'styled-components';
import ModalAlert from '../../components/ModalAlert';
import {Creators as AuthActions} from '../../store/ducks/auth';
import {
  Container,
  Logo,
  Text,
  Button,
  TextInput,
  ViewInput,
  ButtonText,
  PasswordContainer,
  ViewSwitch,
} from './styles';

const Login = () => {
  const data = useSelector(state => state.auth);
  const error = useSelector(state => state.auth.error);
  const dispatch = useDispatch();
  const [email, setEmail] = useState(data.email);
  const [password, setPassword] = useState(data.senha);
  const [passwordVisible, setPasswordVisible] = useState(false);
  const theme = useContext(ThemeContext);
  const inputPassRef = useRef();
  const [isEnabled, setIsEnabled] = useState(data.lembrar);
  const onToggle = () => {
    setIsEnabled(previousState => !previousState);
  };

  function onTryAgain() {
    dispatch(AuthActions.sagaTryAgain());
  }

  useEffect(() => {
    inputPassRef.current.setNativeProps({
      style: {fontFamily: theme.fontFamily.Regular},
    });
  }, []);

  useEffect(() => {
    console.log('changed');
    dispatch(AuthActions.sagaRememberMe(isEnabled, email, password));
    if (isEnabled) {
      dispatch(AuthActions.sagaGetRememberMe());
    }
  }, [dispatch, isEnabled]);

  console.log(data);

  return (
    <Container>
      <ImageBackground
        source={require('../../assets/images/login-background-top.png')}
        style={{resizeMode: 'cover', justifyContent: 'center', flex: 1}}>
        <ModalAlert error={error} onVisible={onTryAgain} />
        <Logo
          source={require('../../assets/images/logo_ioasys-white.png')}
          resizeMode={'contain'}
        />
        <ViewInput>
          <Text>E-mail</Text>
          <TextInput
            placeholder="Ex.: seunome@exemplo.com.br"
            onChangeText={text => setEmail(text)}
            value={email}
            placeholderTextColor={theme.colors.light_text}
            autoCompleteType="email"
            autoCapitalize="none"
          />
        </ViewInput>
        <ViewInput>
          <Text>Senha</Text>
          <PasswordContainer>
            <TextInput
              placeholder="Digite a sua senha"
              onChangeText={text => setPassword(text)}
              value={password}
              secureTextEntry={!passwordVisible}
              placeholderTextColor={theme.colors.light_text}
              autoCompleteType="password"
              autoCapitalize="none"
              ref={inputPassRef}
            />
            <TouchableOpacity
              onPress={() => setPasswordVisible(!passwordVisible)}>
              <Icon
                size={25}
                color={theme.colors.secondary}
                name={passwordVisible ? 'visibility' : 'visibility-off'}
              />
            </TouchableOpacity>
          </PasswordContainer>
        </ViewInput>
        <ViewSwitch>
          <Text style={{marginRight: 10}}>Lembrar de mim</Text>
          <Switch
            trackColor={{
              false: theme.colors.grey,
              true: theme.colors.light_text,
            }}
            thumbColor={
              isEnabled ? theme.colors.secondary : theme.colors.primary
            }
            onValueChange={onToggle}
            value={isEnabled}
          />
        </ViewSwitch>
        <Button
          onPress={() => dispatch(AuthActions.sagaRequest(email, password))}>
          <ButtonText>Entrar</ButtonText>
        </Button>
      </ImageBackground>
    </Container>
  );
};

export default Login;
