import styled from 'styled-components';
import {StyleSheet} from 'react-native';

export const Container = styled.View`
  background-color: ${props => props.theme.colors.background};
  flex: 1;
  justify-content: center;
`;

export const Logo = styled.Image`
  width: 65%;
  height: 15%;
  align-self: center;
`;

export const Text = styled.Text`
  color: ${props => props.theme.colors.secondary};
  margin-left: 5px;
  font-size: 12px;
  font-family: ${props => props.theme.fontFamily.Light};
`;

export const ButtonText = styled(Text)`
  font-size: 14px;
  font-weight: bold;
  color: ${props => props.theme.colors.light_text};
  font-family: ${props => props.theme.fontFamily.Bold};
`;

export const Button = styled.TouchableOpacity`
  background-color: ${props => props.theme.colors.secondary};
  margin: 40px;
  padding: 15px;
  border-radius: 10px;
  align-items: center;
`;

export const PasswordContainer = styled.View`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;

export const TextInput = styled.TextInput`
  color: ${props => props.theme.colors.light_text};
  font-family: ${props => props.theme.fontFamily.Regular};
  padding: 6px;
  width: 90%;
`;

export const ViewInput = styled.View`
  border-bottom-width: ${StyleSheet.hairlineWidth}px;
  border-bottom-color: ${props => props.theme.colors.secondary};
  margin: 20px 20px 0px 20px;
`;

export const ViewSwitch = styled.View`
  margin-left: 22px;
  margin-top: 10px;
  margin-right: 20px;
  align-items: center;
  flex-direction: row;
`;
