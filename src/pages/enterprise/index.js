import React, {useContext, useEffect} from 'react';
import {View} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {useDispatch, useSelector} from 'react-redux';
import {ThemeContext} from 'styled-components';
import LoadingData from '../../components/LoadingData';
import {Creators as EnterpriseActions} from '../../store/ducks/enterprise';

import {
  Container,
  Name,
  Text,
  Description,
  Content,
  SquareType,
  SquareOunEnterprise,
} from './styles';

const Enterprise = ({route}) => {
  const enterprise = useSelector(state => state.enterprise.data);
  const loading = useSelector(state => state.enterprise.loading);
  const dispatch = useDispatch();
  const theme = useContext(ThemeContext);

  useEffect(() => {
    dispatch(EnterpriseActions.sagaRequest(route.params.id));
  }, [dispatch]);

  return loading ? (
    <LoadingData />
  ) : (
    <Container>
      <Content>
        <View style={{marginBottom: 20}}>
          <Name>{enterprise.enterprise_name}</Name>
          <Text>
            {enterprise.city}, {enterprise.country} - Valor:{' '}
            {enterprise.value.toFixed(2)}
          </Text>
        </View>
        <View style={{marginBottom: 20, flexDirection: 'row'}}>
          <SquareType>
            <Text style={{color: theme.colors.dark_text}}>
              {enterprise.enterprise_type.enterprise_type_name}
            </Text>
          </SquareType>
          <SquareOunEnterprise>
            <Text style={{color: theme.colors.dark_text}}>
              Própria:{enterprise.oun_enterprise}
            </Text>
            {enterprise.oun_enterprise ? (
              <Icon name="check" size={30} color={theme.colors.dark_text} />
            ) : (
              <Icon name="clear" size={30} color={theme.colors.dark_text} />
            )}
          </SquareOunEnterprise>
        </View>
        <View style={{marginBottom: 20}}>
          <Text>Telefone:{enterprise.phone}</Text>
          <Text>E-Mail: {enterprise.email}</Text>
        </View>
        <View style={{marginBottom: 20}}>
          <Description>Descrição</Description>
          <Text>{enterprise.description}</Text>
        </View>
        <Description>
          Preço compartilhado: {enterprise.share_price.toFixed(2)}
        </Description>
      </Content>
    </Container>
  );
};

export default Enterprise;
