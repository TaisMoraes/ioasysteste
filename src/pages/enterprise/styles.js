import styled from 'styled-components/native';

export const Container = styled.ScrollView`
  background-color: ${props => props.theme.colors.background};
  flex: 1;
  padding: 10px 20px;
`;

export const Name = styled.Text`
  font-size: 18px;
  color: ${props => props.theme.colors.dark_text};
  font-family: ${props => props.theme.fontFamily.Bold};
`;

export const Description = styled.Text`
  font-size: 14px;
  color: ${props => props.theme.colors.dark_text};
  font-family: ${props => props.theme.fontFamily.Bold};
`;

export const Text = styled.Text`
  font-size: 13px;
  color: ${props => props.theme.colors.inactive_text};
  font-family: ${props => props.theme.fontFamily.Regular};
`;

export const Content = styled.View`
  background-color: ${props => props.theme.colors.background_secondary};
  padding: 15px;
  border-radius: 15px;
`;

export const SquareType = styled.View`
  border-radius: 5px;
  padding: 10px;
  margin-right: 10px;
  background-color: ${props => props.theme.colors.quinternary};
`;
export const SquareOunEnterprise = styled(SquareType)`
  background-color: ${props => props.theme.colors.secondary};
  align-items: center;
`;
