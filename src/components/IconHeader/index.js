import React from 'react';
import {Image} from 'react-native';

const IconHeader = () => {
  return (
    <Image
      source={require('../../assets/images/icon.png')}
      style={{height: 25, width: 25, marginLeft: 10, marginRight: 10}}
      resizeMode="contain"
    />
  );
};

export default IconHeader;
