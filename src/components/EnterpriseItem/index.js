import React, {useContext} from 'react';
import {TouchableOpacity, View} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {ThemeContext} from 'styled-components';
import {Container, Name, Text, Content} from './styles';

const EnterpriseItem = ({item, navigation}) => {
  const theme = useContext(ThemeContext);

  return (
    <Container>
      <TouchableOpacity
        onPress={() => navigation.navigate('Empresa', {id: item.id})}>
        <Content>
          <View>
            <Name>{item.enterprise_name}</Name>
            <Text>{item.enterprise_type.enterprise_type_name}</Text>
            <Text>
              {item.city} - {item.country}
            </Text>
          </View>
          <Icon
            name="keyboard-arrow-right"
            size={20}
            color={theme.colors.quaternary}
          />
        </Content>
      </TouchableOpacity>
    </Container>
  );
};

export default EnterpriseItem;
