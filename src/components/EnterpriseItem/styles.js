import styled from 'styled-components/native';

export const Container = styled.View`
  margin: 10px;
  background-color: ${props => props.theme.colors.background};
  padding: 10px;
  border-radius: 10px;
`;

export const Content = styled.View`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;

export const Text = styled.Text`
  font-size: 12px;
  color: ${props => props.theme.colors.inactive_text};
  font-family: ${props => props.theme.fontFamily.Regular};
`;

export const Name = styled.Text`
  color: ${props => props.theme.colors.dark_text};
  font-family: ${props => props.theme.fontFamily.Regular};
  font-size: 16px;
`;
