import styled from 'styled-components/native';

export const Container = styled.View`
  margin: 5px;
`;

export const Content = styled.View`
  flex-direction: row;
  align-items: center;
`;

export const Text = styled.Text`
  margin-left: 5px;
  color: ${props => props.theme.colors.dark_text};
  font-family: ${props => props.theme.fontFamily.Regular};
`;
