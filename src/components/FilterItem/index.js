import React, {useContext} from 'react';
import {TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {useDispatch} from 'react-redux';
import {ThemeContext} from 'styled-components';
import {Creators as SearchActions} from '../../store/ducks/search';
import {Container, Content, Text} from './styles';

const FilterItem = ({item, onClose, onSelect}) => {
  const dispatch = useDispatch();
  const theme = useContext(ThemeContext);

  const onSelectItem = item => {
    dispatch(SearchActions.sagaSetType(item.id));
    onClose(false);
    onSelect(item.enterprise_type_name);
  };

  return (
    <Container>
      <TouchableOpacity onPress={() => onSelectItem(item)}>
        <Content>
          <Icon name="north-west" size={15} color={theme.colors.quaternary} />
          <Text>{item.enterprise_type_name}</Text>
        </Content>
      </TouchableOpacity>
    </Container>
  );
};

export default FilterItem;
