import React, {useState, useEffect, useContext} from 'react';
import {TouchableOpacity, FlatList} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {useSelector} from 'react-redux';
import {ThemeContext} from 'styled-components';
import FilterItem from '../FilterItem';
import {Container, ContainerList, Text} from './styles';

const Filter = () => {
  const enterprise_types = useSelector(
    state => state.enterprises.enterprise_types,
  );
  const enterpriseType = useSelector(state => state.search.enterprise_type);
  const theme = useContext(ThemeContext);
  const [show, setShow] = useState(false);
  const [name, setName] = useState('Todos');

  useEffect(() => {
    if (enterpriseType === null) {
      setName('Todos');
    }
  }, [enterpriseType]);

  return (
    <>
      <Container>
        <Text>{name}</Text>
        <TouchableOpacity onPress={() => setShow(!show)}>
          <Icon name="filter-list" size={25} color={theme.colors.quaternary} />
        </TouchableOpacity>
      </Container>
      {show && (
        <ContainerList>
          <FlatList
            data={enterprise_types}
            keyExtractor={item => item.id}
            renderItem={({item}) => (
              <FilterItem item={item} onClose={setShow} onSelect={setName} />
            )}
          />
        </ContainerList>
      )}
    </>
  );
};

export default Filter;
