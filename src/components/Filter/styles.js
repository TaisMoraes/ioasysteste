import styled from 'styled-components/native';
import {StyleSheet} from 'react-native';

export const Container = styled.View`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding: 5px 20px 5px 30px;
  border-bottom-width: ${StyleSheet.hairlineWidth}px;
  border-bottom-color: ${props => props.theme.colors.quaternary};
`;

export const ContainerList = styled.View`
  background-color: ${props => props.theme.colors.grey};
  padding: 10px 30px;
  height: 40%;
  border-bottom-left-radius: 20px;
  border-bottom-right-radius: 20px;
`;

export const Text = styled.Text`
  color: ${props => props.theme.colors.quaternary};
  font-family: ${props => props.theme.fontFamily.Regular};
`;
