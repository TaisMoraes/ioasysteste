import styled from 'styled-components/native';

export const Container = styled.View`
  background-color: ${props => props.theme.colors.secondary};
  border-radius: 10px;
  align-items: center;
  padding: 10px;
  margin: 20px;
`;

export const Text = styled.Text`
  color: ${props => props.theme.colors.light_text};
  font-family: ${props => props.theme.fontFamily.Regular};
  padding: 10px;
`;

export const Button = styled.TouchableOpacity`
  background-color: ${props => props.theme.colors.light_text};
  align-items: center;
  margin: 10px;
  padding: 5px;
  border-radius: 10px;
`;

export const TextButton = styled.Text`
  color: ${props => props.theme.colors.red};
  padding: 10px;
  font-family: ${props => props.theme.fontFamily.Regular};
`;

export const ViewButton = styled.View`
  justify-content: space-between;
  align-items: center;
  flex-direction: row;
`;
