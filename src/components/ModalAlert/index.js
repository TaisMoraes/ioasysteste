import React, {useContext} from 'react';
import {Modal, View} from 'react-native';
import {Container, Text, Button, TextButton, ViewButton} from './styles';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {ThemeContext} from 'styled-components';

const ModalAlert = ({error, onVisible}) => {
  const theme = useContext(ThemeContext);
  return (
    <Modal
      animationType="fade"
      transparent={true}
      visible={error !== null}
      onRequestClose={() => {
        onVisible();
      }}>
      <Container>
        <View>
          <Text>{error}</Text>
          <Button onPress={() => onVisible()}>
            <ViewButton>
              <Icon name="error" size={30} color={theme.colors.red} />
              <TextButton>Tentar Novamente</TextButton>
            </ViewButton>
          </Button>
        </View>
      </Container>
    </Modal>
  );
};

export default ModalAlert;
