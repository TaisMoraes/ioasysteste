import React, {useContext, useEffect, useState} from 'react';
import {TextInput, TouchableOpacity, View, Text} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {useDispatch, useSelector} from 'react-redux';
import {ThemeContext} from 'styled-components';
import {Container} from './styles';
import {Creators as SearchActions} from '../../store/ducks/search';

const SearchText = () => {
  const dispatch = useDispatch();
  const [searchText, setSearchText] = useState('');
  const theme = useContext(ThemeContext);
  const enterpriseType = useSelector(state => state.search.enterprise_type);

  useEffect(() => {
    dispatch(SearchActions.sagaSetText(searchText));
  }, [searchText, dispatch]);

  return (
    <Container>
      <Icon name="search" size={20} color={theme.colors.quaternary} />
      <TextInput
        placeholder={'digite o nome da empresa'}
        value={searchText}
        onChangeText={text => setSearchText(text)}
        style={{flex: 2, fontFamily: theme.fontFamily.Regular}}
      />
      {(searchText !== '' || enterpriseType !== null) && (
        <TouchableOpacity
          onPress={() => {
            dispatch(SearchActions.sagaClear());
            setSearchText('');
          }}>
          <Icon name="cancel" size={25} color={theme.colors.quaternary} />
        </TouchableOpacity>
      )}
    </Container>
  );
};

export default SearchText;
