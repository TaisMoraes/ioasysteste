import styled from 'styled-components/native';

export const Container = styled.View`
  flex-direction: row;
  align-items: center;
  margin: 0px 10px 0px 10px;
  padding: 0px 10px 0px 10px;
  background-color: ${props => props.theme.colors.background_secondary};
  border-radius: 120px;
`;
