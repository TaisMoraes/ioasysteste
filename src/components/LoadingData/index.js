import React, {useContext} from 'react';
import {View, ActivityIndicator} from 'react-native';
import {ThemeContext} from 'styled-components';

const LoadingData = () => {
  const theme = useContext(ThemeContext);
  return (
    <View
      style={{backgroundColor: theme.colors.background, flex: 1, padding: 10}}>
      <ActivityIndicator size="large" color={theme.colors.quaternary} />
    </View>
  );
};

export default LoadingData;
