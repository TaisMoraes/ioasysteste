export default {
  colors: {
    primary: '#f2a7b5',
    secondary: '#f2809f',
    tertiary: '#da4b9b',
    quaternary: '#5d238c',
    quinternary: '#4a9d99',
    inactive_text: '#7063b9',
    dark_text: '#36343B',
    light_text: '#fff',
    background: '#282a36',
    background_secondary: '#313341',
    red: '#FF0000',
  },
  fontFamily: {
    Regular: 'NotoSansJP-Regular',
    Bold: 'NotoSansJP-Bold',
    Light: 'NotoSansJP-Light',
  },
};
