export default {
  colors: {
    primary: '#f2a7b5',
    secondary: '#f2809f',
    tertiary: '#da4b9b',
    quaternary: '#5d238c',
    quinternary: '#4a9d99',
    inactive_text: '#7063b9',
    dark_text: '#36343B',
    light_text: '#fff',
    background: '#fcfaff',
    background_secondary: '#f8f3fc',
    red: '#FF0000',
    grey: '#ddd6e3',
  },
  fontFamily: {
    Regular: 'Rubik-Regular',
    Bold: 'Rubik-Bold',
    Light: 'Rubik-Light',
  },
};
