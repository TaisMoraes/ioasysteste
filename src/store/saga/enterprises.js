import {put, call, select} from 'redux-saga/effects';
import {
  apiGetEnterprises,
  apiGetEnterpriseFilter,
} from '../../services/apiEnterprises';
import {TypesReduxEnterprises} from '../ducks/enterprises';

export function* getEnterprises() {
  try {
    const auth = yield select(state => state.auth);
    const search = yield select(state => state.search);

    let response = [];
    let enterpriseTypes = [];

    if (search.name === '' && search.enterprise_type === null) {
      response = yield call(
        apiGetEnterprises,
        auth.access_token,
        auth.client,
        auth.uid,
      );

      response.enterprises.map(element => {
        if (!enterpriseTypes.find(el => el.id === element.enterprise_type.id)) {
          enterpriseTypes = [...enterpriseTypes, element.enterprise_type];
        }
      });

      yield put({
        type: TypesReduxEnterprises.LOAD_ENTERPRISE_TYPES,
        payload: enterpriseTypes.sort((a, b) => {
          return a.id - b.id;
        }),
      });
    } else {
      response = yield call(
        apiGetEnterpriseFilter,
        auth.access_token,
        auth.client,
        auth.uid,
        search.enterprise_type,
        search.name,
      );
    }

    yield put({
      type: TypesReduxEnterprises.LOAD,
      payload: response.enterprises,
    });
  } catch (error) {
    yield put({tupe: TypesReduxEnterprises.FAIL, error: error});
  }
}
