import {put, call} from 'redux-saga/effects';
import {apiAuth} from '../../services/apiAuth';
import {TypesReduxAuth} from '../ducks/auth';
import AsyncStorage from '@react-native-community/async-storage';

export function* getAuth(action) {
  try {
    const response = yield call(apiAuth, action.email, action.password);

    if (response.status === 200) {
      yield put({
        type: TypesReduxAuth.LOAD,
        access_token: response.headers['access-token'],
        client: response.headers.client,
        uid: response.headers.uid,
      });
    } else {
      throw response;
    }
  } catch (error) {
    yield put({type: TypesReduxAuth.FAIL, error: error});
  }
}

export function* tryAgain() {
  try {
    yield put({type: TypesReduxAuth.CLEAR});
  } catch (error) {}
}

export function* logout() {
  try {
    yield put({type: TypesReduxAuth.CLEAR});
  } catch (error) {}
}

export function* rememberMe(action) {
  try {
    const enabledString = action.enabled.toString();
    console.log(action);
    if (action.enabled) {
      yield AsyncStorage.setItem('@email', action.email);
      yield AsyncStorage.setItem('@senha', action.senha);
      yield AsyncStorage.setItem('@lembrar', enabledString);
    } else if (!action.enabled) {
      AsyncStorage.removeItem('@email');
      AsyncStorage.removeItem('@senha');
      AsyncStorage.removeItem('@lembrar');
      yield put({
        type: TypesReduxAuth.LOAD_REMEMBER_ME,
        email: '',
        senha: '',
        lembrar: false,
      });
    }
  } catch (error) {
    console.log(error);
  }
}

export function* getRememberMe() {
  try {
    const email = yield AsyncStorage.getItem('@email');
    const senha = yield AsyncStorage.getItem('@senha');
    const lembrar = yield AsyncStorage.getItem('@lembrar');
    let lembrarBool = false;
    lembrar !== null && (lembrarBool = lembrar === 'true' ? true : false);

    console.log(lembrar, email, senha);

    if (lembrar) {
      yield put({
        type: TypesReduxAuth.LOAD_REMEMBER_ME,
        email: email === null ? '' : email,
        senha: senha === null ? '' : senha,
        lembrar: lembrarBool,
      });
    } else {
      yield put({
        type: TypesReduxAuth.LOAD_REMEMBER_ME,
        email: '',
        senha: '',
        lembrar: false,
      });
    }
  } catch (error) {
    console.log(error);
  }
}
