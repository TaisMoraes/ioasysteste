import {put} from 'redux-saga/effects';
import {TypesReduxSearch} from '../ducks/search';

export function* setText(action) {
  try {
    yield put({
      type: TypesReduxSearch.LOAD_TEXT,
      name: action.name,
    });
  } catch (error) {
    console.log(error);
  }
}

export function* setType(action) {
  try {
    yield put({
      type: TypesReduxSearch.LOAD_TYPE,
      enterprise_type: action.enterprise_type,
    });
  } catch (error) {
    console.log(error);
  }
}

export function* clear() {
  try {
    yield put({
      type: TypesReduxSearch.CLEAR,
    });
  } catch (error) {
    console.log(error);
  }
}
