import {put, call, select} from 'redux-saga/effects';
import {apiGetEnterpriseByID} from '../../services/apiEnterprises';
import {TypesReduxEnterprise} from '../ducks/enterprise';

export function* getEnterpriseByID(action) {
  try {
    yield put({
      type: TypesReduxEnterprise.CLEAR,
    });

    const auth = yield select(state => state.auth);

    const response = yield call(
      apiGetEnterpriseByID,
      auth.access_token,
      auth.client,
      auth.uid,
      action.id,
    );

    yield put({
      type: TypesReduxEnterprise.LOAD,
      payload: response.enterprise,
    });
  } catch (error) {
    yield put({tupe: TypesReduxEnterprise.FAIL, error: error});
  }
}
