export const TypesReduxSearch = {
  SAGA_SET_TYPE: 'search/SAGA_SET_TYPE',
  SAGA_SET_TEXT: 'search/SAGA_SET_TEXT',
  SAGA_CLEAR: 'search/SAGA_CLEAR',
  LOAD_TYPE: 'search/LOAD_TYPE',
  LOAD_TEXT: 'search/LOAD_TEXT',
  CLEAR: 'search/CLEAR',
};

const INITIAL_STATE = {
  enterprise_type: null,
  name: '',
};

export default function search(state = INITIAL_STATE, action) {
  switch (action.type) {
    case TypesReduxSearch.LOAD_TEXT:
      return {
        ...state,
        name: action.name,
      };
    case TypesReduxSearch.LOAD_TYPE:
      return {
        ...state,
        enterprise_type: action.enterprise_type,
      };
    case TypesReduxSearch.CLEAR:
      return {
        ...state,
        name: '',
        enterprise_type: null,
      };
    default:
      return state;
  }
}

export const Creators = {
  sagaSetType: enterprise_type => ({
    type: TypesReduxSearch.SAGA_SET_TYPE,
    enterprise_type: enterprise_type,
  }),
  sagaSetText: name => ({
    type: TypesReduxSearch.SAGA_SET_TEXT,
    name: name,
  }),
  sagaClear: () => ({type: TypesReduxSearch.SAGA_CLEAR}),
};
