export const TypesReduxEnterprises = {
  SAGA_REQUEST: 'enterprises/SAGA_REQUEST',
  LOAD: 'enterprises/LOAD',
  LOAD_ENTERPRISE_TYPES: 'enterprises/LOAD_ENTERPRISE_TYPES',
  CLEAR: 'enterprises/CLEAR',
  FAIL: 'enterprises/FAIL',
};

const INITIAL_STATE = {
  data: [],
  enterprise_types: [],
  error: null,
  loading: true,
};

export default function auth(state = INITIAL_STATE, action) {
  switch (action.type) {
    case TypesReduxEnterprises.LOAD:
      return {
        ...state,
        data: action.payload,
        loading: false,
      };
    case TypesReduxEnterprises.LOAD_ENTERPRISE_TYPES:
      return {
        ...state,
        enterprise_types: action.payload,
      };
    case TypesReduxEnterprises.CLEAR:
      return {
        ...state,
        data: [],
        erro: null,
        loading: true,
      };
    case TypesReduxEnterprises.FAIL:
      return {
        ...state,
        data: [],
        erro: action.error,
        loading: true,
      };
    default:
      return state;
  }
}

export const Creators = {
  sagaRequest: () => ({
    type: TypesReduxEnterprises.SAGA_REQUEST,
  }),
};
