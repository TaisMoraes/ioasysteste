import {combineReducers} from 'redux';
import auth from './auth';
import enterprises from './enterprises';
import enterprise from './enterprise';
import search from './search';

export default combineReducers({auth, enterprises, enterprise, search});
