export const TypesReduxAuth = {
  SAGA_REQUEST: 'auth/SAGA_REQUEST',
  SAGA_TRY_AGAIN: 'auth/SAGA_TRY_AGAIN',
  SAGA_LOGOUT: 'auth/SAGA_LOGOUT',
  SAGA_REMEMBER_ME: 'auth/SAGA_REMEMBER_ME',
  SAGA_GET_REMEMBER_ME: 'auth/SAGA_GET_REMEMBER_ME',
  LOAD: 'auth/LOAD',
  LOAD_REMEMBER_ME: 'auth/LOAD_REMEMBER_ME',
  CLEAR: 'auth/CLEAR',
  FAIL: 'auth/FAIL',
};

const INITIAL_STATE = {
  access_token: '',
  client: '',
  uid: '',
  error: null,
  email: '',
  senha: '',
  lembrar: false,
};

export default function auth(state = INITIAL_STATE, action) {
  switch (action.type) {
    case TypesReduxAuth.LOAD:
      return {
        ...state,
        access_token: action.access_token,
        client: action.client,
        uid: action.uid,
      };
    case TypesReduxAuth.CLEAR:
      return {
        ...state,
        access_token: '',
        client: '',
        uid: '',
        error: null,
      };
    case TypesReduxAuth.LOAD_REMEMBER_ME:
      return {
        ...state,
        email: action.email,
        senha: action.senha,
        lembrar: action.lembrar,
      };
    case TypesReduxAuth.FAIL:
      return {
        ...state,
        access_token: '',
        client: '',
        uid: '',
        error: action.error,
      };
    default:
      return state;
  }
}

export const Creators = {
  sagaRequest: (email, password) => ({
    type: TypesReduxAuth.SAGA_REQUEST,
    email: email,
    password: password,
  }),
  sagaRememberMe: (enabled, email, password) => ({
    type: TypesReduxAuth.SAGA_REMEMBER_ME,
    enabled: enabled,
    email: email,
    senha: password,
  }),
  sagaGetRememberMe: () => ({
    type: TypesReduxAuth.SAGA_GET_REMEMBER_ME,
  }),
  sagaTryAgain: () => ({
    type: TypesReduxAuth.SAGA_TRY_AGAIN,
  }),
  sagaLogout: () => ({
    type: TypesReduxAuth.SAGA_LOGOUT,
  }),
};
