export const TypesReduxEnterprise = {
  SAGA_REQUEST: 'enterprise/SAGA_REQUEST',
  LOAD: 'enterprise/LOAD',
  CLEAR: 'enterprise/CLEAR',
  FAIL: 'enterprise/FAIL',
};

const INITIAL_STATE = {
  data: {},
  error: null,
  loading: true,
};

export default function auth(state = INITIAL_STATE, action) {
  switch (action.type) {
    case TypesReduxEnterprise.LOAD:
      return {
        ...state,
        data: action.payload,
        loading: false,
      };
    case TypesReduxEnterprise.CLEAR:
      return {
        ...state,
        data: {},
        erro: null,
        loading: true,
      };
    case TypesReduxEnterprise.FAIL:
      return {
        ...state,
        data: {},
        erro: action.error,
        loading: true,
      };
    default:
      return state;
  }
}

export const Creators = {
  sagaRequest: id => ({
    type: TypesReduxEnterprise.SAGA_REQUEST,
    id: id,
  }),
};
