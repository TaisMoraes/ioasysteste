import {takeLatest, all} from 'redux-saga/effects';
import {TypesReduxAuth} from './ducks/auth';
import {TypesReduxEnterprise} from './ducks/enterprise';
import {TypesReduxEnterprises} from './ducks/enterprises';
import {TypesReduxSearch} from './ducks/search';
import {
  getAuth,
  getRememberMe,
  logout,
  rememberMe,
  tryAgain,
} from './saga/auth';
import {getEnterpriseByID} from './saga/enterprise';
import {getEnterprises} from './saga/enterprises';
import {clear, setText, setType} from './saga/search';

export default function* root() {
  yield all([takeLatest(TypesReduxAuth.SAGA_REQUEST, getAuth)]);
  yield all([takeLatest(TypesReduxAuth.SAGA_TRY_AGAIN, tryAgain)]);
  yield all([takeLatest(TypesReduxAuth.SAGA_LOGOUT, logout)]);
  yield all([takeLatest(TypesReduxAuth.SAGA_GET_REMEMBER_ME, getRememberMe)]);
  yield all([takeLatest(TypesReduxAuth.SAGA_REMEMBER_ME, rememberMe)]);
  yield all([takeLatest(TypesReduxEnterprises.SAGA_REQUEST, getEnterprises)]);
  yield all([takeLatest(TypesReduxEnterprise.SAGA_REQUEST, getEnterpriseByID)]);
  yield all([takeLatest(TypesReduxSearch.SAGA_SET_TEXT, setText)]);
  yield all([takeLatest(TypesReduxSearch.SAGA_SET_TYPE, setType)]);
  yield all([takeLatest(TypesReduxSearch.SAGA_CLEAR, clear)]);
}
